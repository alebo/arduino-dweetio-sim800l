# Arduino DweetIO SIM800L

An example Arduino "sketch" wherein Arduino Mega 2560 rev3 communicates with Dweet.io using SIM800L GSM/GPRS module. Arduino first sends data to Dweet.io, then receives "instructions" from Dweet.io. Used libraries are FONA library, and Jsmn library.